'''
@author:
'''

from bottle import Bottle, template, debug, static_file,redirect,request,response
import sqlite3
import interface
import users
import os
from database import COMP249Db

DB_NAME = 'comp249.db'
COOKIE_NAME = 'sessionid'
UPLOAD_DIR = 'static/images/'
application = Bottle()


@application.route('/')
def default():
    conn = sqlite3.connect(DB_NAME)
    # Get the 3 latest uploaded images
    image_list = interface.list_images(conn,3)
    # Check if a user has logged in (Determines whether to display login or logout form)
    if users.session_user(conn) is not None:
        return template('index.tpl', images=image_list, logged_in=True, session_user=users.session_user(conn))
    return template('index.tpl', images=image_list, logged_in=False)


@application.route('/about')
def about():
    conn = sqlite3.connect(DB_NAME)
    # Check if a user has logged in (Determines whether to display login or logout form)
    if users.session_user(conn) is not None:
        return template('about.tpl', logged_in=True, session_user=users.session_user(conn))
    return template('about.tpl', logged_in=False)


@application.route('/my')
def my_images():
    conn = sqlite3.connect(DB_NAME)
    usernick = users.session_user(conn)
    # Prevent people who aren't logged in from accessing my images page
    if usernick is None:
        redirect('/')
    # Get the 3 latest images that the logged in user has uploaded.
    image_list = interface.list_images(conn, 3, usernick)
    return template('my.tpl', images=image_list, session_user=users.session_user(conn), logged_in=True)


@application.route('/static/<filename>')
def serve_static(filename):
    return static_file(filename, root='./static/')


@application.route('/static/images/<filename>')
def serve_static_images(filename):
    return static_file(filename, root='./static/images/')


@application.route('/like',method='POST')
def like_handler():
    conn = sqlite3.connect(DB_NAME)
    filename = request.forms.get('filename')
    interface.add_like(conn, filename)
    redirect('/')


@application.route('/login',method='POST')
def login_handler():
    usernick = request.forms.get('nick')
    password = request.forms.get('password')
    login_successful = users.check_login(COMP249Db(), usernick, password)
    # If login was successful start a new session
    if login_successful:
        users.generate_session(COMP249Db(), usernick)
    # If the login failed return the page with the failed login message
    else:
        conn = sqlite3.connect(DB_NAME)
        image_list = interface.list_images(conn, 3)
        return template('index.tpl', images=image_list, logged_in=False, login_state="Login Failed!")
    redirect('/')


@application.route('/logout',method='POST')
def logout_handler():
    conn = sqlite3.connect(DB_NAME)
    user_nick = request.forms.get('session_user')
    users.delete_session(conn, user_nick)
    redirect('/')


@application.route('/upload',method='POST')
def upload_handler():
    conn = sqlite3.connect(DB_NAME)
    imagefile = request.files.get('imagefile')
    usernick = users.session_user(conn)
    # Prevent upload attempt from people who are not logged in
    if usernick is None:
        redirect('/')
    # If no file has been selected then prevent upload attempt
    if imagefile is None:
        redirect('/my')
    # Only allow jpeg files to be uploaded
    if imagefile.content_type != 'image/jpeg':
        redirect('/my')
    # Add the image to the database and then add it to the static/images folder
    interface.add_image(conn, imagefile.filename, usernick)
    save_path = os.path.join(UPLOAD_DIR, imagefile.filename)
    imagefile.save(save_path,overwrite=True)
    redirect('/my')

# As a user when browse my images I want to be able to delete images I have uploaded
# from the website by clicking the delete button below the like button for the image I
# want to delete.
@application.route('/delete',method='POST')
def delete_handler():
    conn = sqlite3.connect(DB_NAME)
    filename = request.forms.get('filename')
    usernick = users.session_user(conn)
    # Prevent delete attempt from people who are not logged in
    if usernick is None:
        redirect('/')
    # Remove the image from the database (Also checks that the image belongs to the current
    # user to prevent deletion of other users files.
    interface.remove_image(conn, filename, usernick)
    # NOTE: The actual image files aren't being deleted from the static/images folder because
    # this would mess up the the default database if any of the starting images files were
    # deleted, but in production it would make sense to also remove deleted images from
    # the static/images folder since they are no longer in use.
    redirect('/my')

# As a user when I attempt to access a page that does not exist, I want to
# taken to a page that ackownledges this and contains the navigation bar
# so that I can navigate to a page that does exist.
@application.error(404)
def page_not_found(error):
    # Sends any requests for pages that do not exist to a 404 page.
    conn = sqlite3.connect(DB_NAME)
    if users.session_user(conn) is not None:
        return template('404.tpl',logged_in=True, session_user=users.session_user(conn))
    return template('404.tpl',logged_in=False)


# As a user who does not have an account already when I visit any page on
# the website I want to be able to click the sign up link and be taken
# to a page that allows me to create a new account, after creating an
# account I should be returned to the homepage and logged into the
# newly created account
@application.route('/signup')
def signup():
    conn = sqlite3.connect(DB_NAME)
    # Block logged in users from creating another account
    if users.session_user(conn) is not None:
        redirect('/')
    return template('signup.tpl', logged_in=False)

# As a user who does not have an account already when I visit any page on
# the website I want to be able to click the sign up link and be taken
# to a page that allows me to create a new account, after creating an
# account I should be returned to the homepage and logged into the
# newly created account
@application.route('/createaccount',method='POST')
def handle_signup():
    conn = sqlite3.connect(DB_NAME)
    # Get new nick and password
    usernick = request.forms.get('nick')
    password = request.forms.get('password')
    # Block logged in users from creating another account
    if users.session_user(conn) is not None:
        redirect('/')
    # Block blank (or short) usernames and passwords, and return page acknowledging reason
    if len(usernick) < 3:
        return template('signup.tpl', logged_in=False, account_creation_state="Username must be atleast 3 characters.")
    if len(password) < 3:
        return template('signup.tpl', logged_in=False, account_creation_state="Password must be atleast 3 characters.")
    # If the account name already exists, prevent new sign up and return page acknowledging reason
    if interface.account_exists(conn,usernick):
        return template('signup.tpl', logged_in=False, account_creation_state="Username already in use.")
    # Insert account into the database (password is encoded and avatar created in this function)
    interface.create_account(conn, usernick, password)
    # Perform a login check and then log the user into the newly created account
    login_successful = users.check_login(COMP249Db(), usernick, password)
    # If login was successful start a new session
    if login_successful:
        users.generate_session(COMP249Db(), usernick)
    redirect('/')


if __name__ == '__main__':
    debug()
    application.run()
