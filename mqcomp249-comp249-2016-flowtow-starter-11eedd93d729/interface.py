'''
@author:
'''

import datetime
from database import COMP249Db

def list_images(db, n, usernick=None):
    """Return a list of dictionaries for the first 'n' images in
    order of timestamp. Each dictionary will contain keys 'filename', 'timestamp', 'user' and 'likes'.
    The 'likes' value will be a count of the number of likes for this image as returned by count_likes.
    If usernick is given, then only images belonging to that user are returned."""
    cursor = db.cursor()
    results = []
    for row in cursor.execute("SELECT * FROM images"):
        if usernick is None:
            # Construct a dictionary from the sql query
            row_dict = {
                "filename": row[0],
                "timestamp": row[1],
                "user": row[2],
                "likes": count_likes(db,row[0])
            }
            results.append(row_dict)
        else:
            if row[2] == usernick:
                row_dict = {
                    "filename": row[0],
                    "timestamp": row[1],
                    "user": row[2],
                    "likes": count_likes(db, row[0])
                }
                results.append(row_dict)
    # Sort the results of the query in descending order (Latest first) based on the time stamp
    results_sorted = sorted(results,key=lambda k: k['timestamp'],reverse=True)
    # Only return 0:n since we only want n images to be returned
    results_first_n = results_sorted[0:n]
    return results_first_n


def add_image(db, filename, usernick):
    """Add this image to the database for the given user"""
    cursor = db.cursor()
    cursor.execute("INSERT INTO images VALUES ('{}','{}','{}')".format(filename, datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"), usernick))
    db.commit()
    return


def add_like(db, filename, usernick=None):
    """Increment the like count for this image"""
    cursor = db.cursor()
    # Make sure the file exists, otherwise return without adding like
    cursor.execute("SELECT * FROM images WHERE filename = '{}'".format(filename))
    if cursor.fetchone() is None:
        return
    # Make sure user exists (if it is not an anonymous like), otherwise return without adding like
    if usernick is not None:
        cursor.execute("SELECT * FROM users WHERE nick = '{}'".format(usernick))
        if cursor.fetchone() is None:
            return
    # Add the like to the database
    cursor.execute("INSERT INTO likes VALUES ('{}','{}')".format(filename, usernick))
    db.commit()
    return


def count_likes(db, filename):
    """return the number of likes for a filename"""
    cursor = db.cursor()
    cursor.execute("SELECT count(*) FROM likes WHERE likes.filename = '{}'".format((filename)))
    result = cursor.fetchone()
    return result[0]


def remove_image(db, filename, usernick):
    cursor = db.cursor()
    # Remove image from image table
    cursor.execute("DELETE FROM images WHERE images.filename ='{}' AND images.usernick='{}'".format(filename,usernick))
    # Remove all the likes that the image had from the like table
    cursor.execute("DELETE FROM likes WHERE likes.filename ='{}'".format(filename))
    db.commit()
    return


def account_exists(db, usernick):
    cursor = db.cursor()
    cursor.execute("SELECT * FROM users WHERE users.nick = '{}'".format((usernick)))
    result = cursor.fetchone()
    # If the query returned a result the account already exists
    if result is not None:
        return True
    return False


def create_account(db, usernick, password):
    cursor = db.cursor()
    #Insert usernick, encoded version of chosen password and avatar into the users table
    cursor.execute("INSERT INTO users VALUES ('{}','{}','{}')".format(usernick, COMP249Db.encode(COMP249Db,password),"http://robohash.org/" + usernick))
    db.commit()
    return
