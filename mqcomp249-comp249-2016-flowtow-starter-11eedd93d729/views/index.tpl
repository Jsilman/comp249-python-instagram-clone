% rebase('base.tpl')

	<div class="Center">
        % for item in images:
            <div class="flowtow">
                <img src="/static/images/{{item['filename']}}"/>
				<div class="FlowtowInformation">
					<div class="Middle">
						<p class="user">{{item['user']}}</p>
						<p class="date">{{item['timestamp']}}</p>
						<p class="likes">{{item['likes']}} Likes</p>
						<form action="/like" method="post">
                            <input type="hidden" name="filename" value="{{item['filename']}}">
							<input type="hidden" name="username">
							<input type="submit" value="Like" class="addLike">
						</form>
					</div>
				</div>
            </div>
        % end
	</div>