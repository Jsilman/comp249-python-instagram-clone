<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flowtow</title>
    <link rel='stylesheet' href='/static/style.css' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
</head>

    <body>

        <div class="Center">
            <div class="textPanel">
                <p>Welcome to FlowTow</p>
            </div>
        </div>

        <div class="Center">
            <Nav>
                <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/about">About</a></li>
                % if logged_in:
                    <li><a href="/my">My Images</a></li>
                % else:
                    <li><a href="/signup">Sign Up</a></li>
                % end
                </ul>
                % if logged_in:
                    <form id="logoutform" action="/logout" method="post">
                        Logged in as {{session_user}}
                        <input type="hidden" name="session_user" value="{{session_user}}">
                        <input type="submit" value="Logout">
                    </form>
                % else:
                    <form id="loginform" action="/login" method="post">
                        {{get('login_state', '')}}
                        <input type="text" name="nick" placeholder="Username">
                        <input type="text" name="password" placeholder="Password">
                        <input type="submit" value="Login" class="loginButton">
                    </form>
                % end
            </Nav>
        </div>

        <div>
         {{!base}}
        </div>

    </body>
</html>
