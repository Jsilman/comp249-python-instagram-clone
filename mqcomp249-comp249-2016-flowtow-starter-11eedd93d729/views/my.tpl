% rebase('base.tpl')

    <div class="Center">
        <div class="textPanel">
            <form id="uploadform" action="/upload" method="post" enctype="multipart/form-data">
                Upload an image to your gallery
                <label class="uploadButton">
                    <input type="file" name="imagefile">
                    Choose Image
                </label>
                <input type="submit" value="Upload">
		    </form>
        </div>
    </div>

	<div class="Center">
        % for item in images:
            <div class="flowtow">
                <img src="/static/images/{{item['filename']}}"/>
				<div class="FlowtowInformation">
					<div class="Middle">
						<p class="user">{{item['user']}}</p>
						<p class="date">{{item['timestamp']}}</p>
						<p class="likes">{{item['likes']}} Likes</p>
						<form action="/like" method="post">
                            <input type="hidden" name="filename" value="{{item['filename']}}">
							<input type="hidden" name="username">
							<input type="submit" value="Like" class="addLike">
						</form>
						<form action="/delete" method="post">
						    <input type="hidden" name="filename" value="{{item['filename']}}">
							<input type="submit" value="Delete" class="DeleteImage">
						</form>
					</div>
				</div>
            </div>
        % end
	</div>