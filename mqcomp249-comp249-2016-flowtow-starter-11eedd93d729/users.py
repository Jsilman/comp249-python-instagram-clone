'''
@author:
'''

import bottle
import random
import uuid

# this variable MUST be used as the name for the cookie used by this application
COOKIE_NAME = 'sessionid'


def check_login(db, usernick, password):
    """returns True if password matches stored"""
    cursor = db.cursor()
    # Get the encoded version of the password
    password = db.encode(password)
    # Select any matching nick and password
    cursor.execute("SELECT * FROM users WHERE users.nick = '{}' AND users.password = '{}'".format(usernick,password))
    # If a match has been found return true else return false
    if cursor.fetchone() is not None:
        return True
    else:
        return False


def generate_session(db, usernick):
    """create a new session and add a cookie to the response object (bottle.response)
    user must be a valid user in the database, if not, return None
    There should only be one session per user at any time, if there
    is already a session active, use the existing sessionid in the cookie
    """
    cursor = db.cursor()
    # Check if user exists, if not return None
    cursor.execute("SELECT * FROM users WHERE users.nick = '{}'".format(usernick))
    result = cursor.fetchone()
    if result is None:
        return None
    # Check if the user already has a session and if so return it
    cursor.execute("SELECT * FROM sessions WHERE sessions.usernick = '{}'".format(usernick))
    result = cursor.fetchone()
    if result is not None:
        return result[1]
    # Generate a random session id and insert it into the database
    session_id = uuid.uuid4()
    cursor.execute("INSERT INTO sessions VALUES ('{}','{}')".format(session_id, usernick))
    cursor.close()
    db.commit()
    # Set the cookie
    bottle.response.set_cookie(COOKIE_NAME, str(session_id))

    return session_id


def delete_session(db, usernick):
    """remove all session table entries for this user"""
    cursor = db.cursor()
    cursor.execute("DELETE FROM sessions WHERE sessions.usernick =  '{}'".format(usernick))
    db.commit()
    return


def session_user(db):
    """try to
    retrieve the user from the sessions table
    return usernick or None if no valid session is present"""
    cookie = bottle.request.get_cookie(COOKIE_NAME)
    # If no cookie is found return none
    if cookie is None:
        return None
    # Check the database for the session id found in the cookie
    cursor = db.cursor()
    cursor.execute("SELECT * FROM sessions WHERE sessions.sessionid = '{}'".format(cookie))
    result = cursor.fetchone()
    if result is None:
        return None
    else:
        return result[1]